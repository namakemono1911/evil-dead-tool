/*****************************************************************************
save.cpp
Aythor	: 上野　匠
Data	: 2017_08_27
=============================================================================
Updata

*****************************************************************************/
//////////////////////////////////////////////////////////////////////////////
//ヘッダーファイルインクルード
//////////////////////////////////////////////////////////////////////////////
#include "main.h"
#include "scene.h"
#include "manager.h"
#include "motionEdit.h"
#include "ui.h"
#include "save.h"

//////////////////////////////////////////////////////////////////////////////
//定数定義
//////////////////////////////////////////////////////////////////////////////
#define ADD_DIR	"\\data"	//カレントディレクトリから追加のディレクトリ

//////////////////////////////////////////////////////////////////////////////
//静的メンバ変数初期化
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//関数名	: Save::saveFile
//返り値	: 
//説明		: 保存するディレクトリを設定
//////////////////////////////////////////////////////////////////////////////
string Save::selectSaveFileDir(HWND wnd, LPCSTR title, LPCSTR extension, LPCSTR extensionFilter)
{
	char saveFileDir[FILE_NAME_SIZE] = "\0";
	auto openFileName = settingOpenfile(wnd, title, extension, extensionFilter, ADD_DIR);
	openFileName.lpstrFile = saveFileDir;

	//保存処理
	if (!GetSaveFileName(&openFileName))
		return string("\0");

	return string(saveFileDir);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Save::saveTransform
//返り値	: 
//説明		: transformを保存
//////////////////////////////////////////////////////////////////////////////
void Save::saveTransform(FILE * fp, const Transform transform)
{
	fprintf(fp, "\tpos\t\t= %f %f %f\n", transform.pos.x, transform.pos.y, transform.pos.z);
	fprintf(fp, "\trot\t\t= %f %f %f\n", transform.rot.x, transform.rot.y, transform.rot.z);
	fprintf(fp, "\tsize\t= %f %f %f\n", transform.size.x, transform.size.y, transform.size.z);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Save::saveCollisionBox
//返り値	: 
//説明		: コリジョンボックス保存
//////////////////////////////////////////////////////////////////////////////
void Save::saveCollisionBox(FILE * fp, CollisionBox * col)
{
	auto	box = (CollisionBox*)col;
	fprintf(fp, "COLLISION_BOX\n");
	fprintf(fp, "{\n");
	saveTransform(fp, Transform(box->getPos(), box->getRot(), box->getSize()));
	fprintf(fp, "}\n");
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Save::saveCollisionBall
//返り値	: 
//説明		: コリジョンボール保存
//////////////////////////////////////////////////////////////////////////////
void Save::saveCollisionBall(FILE * fp, CollisionBall * col)
{
	auto	ball = (CollisionBall*)col;
	fprintf(fp, "COLLISION_BALL\n");
	fprintf(fp, "{\n");
	fprintf(fp, "\tpos\t\t= %f %f %f\n", ball->getPos().x, ball->getPos().y, ball->getPos().z);
	fprintf(fp, "\tlen\t\t= %f\n", ball->getLen());
	fprintf(fp, "}\n");
}
//////////////////////////////////////////////////////////////////////////////
//関数名	: Save::saveEachType
//返り値	: 
//説明		: タイプごと保存
//////////////////////////////////////////////////////////////////////////////
void Save::saveEachType(FILE * fp, Collision::TYPE type, Collision * col)
{
	switch (type)
	{
	case COLLISION_BOX:
		saveCollisionBox(fp, (CollisionBox*)col);
		break;

	case COLLISION_BALL:
		saveCollisionBall(fp, (CollisionBall*)col);
		break;
	}
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Save::saveCollisionList
//返り値	: 
//説明		: コリジョンリスト保存
//////////////////////////////////////////////////////////////////////////////
void Save::saveCollisionList(FILE * fp, const list<Collision*> colList)
{
#pragma omp parallel for
	for (auto colIt = colList.begin(); colIt != colList.end(); ++colIt)
	{
		auto	col = *colIt;
		saveEachType(fp, col->getType(), col);
	}
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Save::saveCollisionSceneHas
//返り値	: 
//説明		: シーンが持っているコリジョン保存
//////////////////////////////////////////////////////////////////////////////
void Save::saveCollision(FILE * fp, Scene* scene)
{
	auto	colManager = scene->getCollision();
	if (colManager == NULL)
		return;

	saveCollisionList(fp, *colManager->getCollisionList());
}

/*
#################################################################################################################################################
SaveMap class
#################################################################################################################################################
*/
//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveMap::saveObject
//返り値	: 
//説明		: オブジェクト情報保存
//////////////////////////////////////////////////////////////////////////////
void SaveMap::saveFile(string fileName)
{
	if (fileName == "\0")
		return;

	FILE *fp = fopen(fileName.c_str(), "w");
	if (fp == NULL)
	{
		fileName += "を保存できませんでした";
		MessageBox(NULL, fileName.c_str(), "error", MB_OK);
		return;
	}
	//オブジェクトとフィールド保存
	saveObject(fp);
	saveField(fp);

	fclose(fp);
	MessageBox(NULL, "保存完了", "メッセージ", MB_OK);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveMap::saveObject
//返り値	: 
//説明		: オブジェクト情報保存
//////////////////////////////////////////////////////////////////////////////
void SaveMap::saveObject(FILE * fp)
{
	//ヘッダーコメント
	fprintf(fp, "#####################################################\n");
	fprintf(fp, "# オブジェクトの配置\n");
	fprintf(fp, "#####################################################\n");
	
	auto	objectList = Scene::getTypeList(Scene::OBJECT);
	for (auto it = objectList.begin(); it != objectList.end(); it++)
	{
		auto scene = *it;

		//テキスト書き込み内容
		fprintf(fp, "SET_OBJECT %s\n{\n", scene->getMyName().c_str());

		//位置情報保存
		saveTransform(fp, scene->getTransform());

		//コリジョン情報保存
		saveCollision(fp, scene);

		fprintf(fp, "}\n\n");
	}
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveMap::saveField
//返り値	: 
//説明		: フィールド情報保存
//////////////////////////////////////////////////////////////////////////////
void SaveMap::saveField(FILE * fp)
{
	//ヘッダーコメント
	fprintf(fp, "#####################################################\n");
	fprintf(fp, "# フィールド(地面）の配置\n");
	fprintf(fp, "#####################################################\n");

	//リスト探索
	auto	ui = (MapEditorUi*)Manager::getUi();
	auto	fieldList = ui->getStageList()->getFieldList();
	ui = NULL;

	for (auto it = fieldList->begin(); it != fieldList->end(); ++it)
	{
		//情報収集
		auto field = *it;
		if (field->getObjType() != Scene::FIELD)
			continue;

		Transform	transform = field->getTransform();
		Dcolor color = *field->getColorAddr();
		Tyle	numTyle = field->getNumTyle();
		string	texName = field->getTextureNameList()[field->getTextureId()];

		fprintf(fp, "SET_FIELD %s\n{\n", texName.c_str());

		//位置情報保存
		fprintf(fp, "\tpos\t\t= %f %f %f\n", transform.pos.x, transform.pos.y, transform.pos.z);
		fprintf(fp, "\trot\t\t= %f %f %f\n", transform.rot.x, transform.rot.y, transform.rot.z);
		fprintf(fp, "\tsize\t= %f %f\n", field->getTyleSize().x, field->getTyleSize().y);

		//タイル数保存
		fprintf(fp, "\tnumTyle\t= %d %d\n", numTyle.x, numTyle.y);

		//頂点色保存
		fprintf(fp, "\tcolor\t= %f %f %f %f\n", color.r, color.g, color.b, color.a);

		fprintf(fp, "}\n\n");
	}
}

/*
#################################################################################################################################################
SaveMotion class
#################################################################################################################################################
*/
////////////////////////////////////////////////////////////////////////////////
////関数名	: SaveMotion::saveFile
////返り値	: 
////説明		: 保存処理
////////////////////////////////////////////////////////////////////////////////
//void SaveMotion::saveFile(string fileName)
//{
//	if (fileName == "\0")
//		return;
//
//	FILE	*fp = fopen(fileName.c_str(), "w");
//	fp = fopen(fileName.c_str(), "a");
//
//	//ヘッダーコメント
//	fprintf(fp, "#####################################################\n");
//	fprintf(fp, "# モデルの保存\n");
//	fprintf(fp, "#####################################################\n");
//
//	auto	ui = (MotionEditorUi*)Manager::getUi();
//	auto	modelParts = ui->getParts();
//	ui = NULL;
//
//	//パーツの情報保存
//	savePartsInfomation(fp, modelParts->getParts().mMotionDataList);
//
//	fclose(fp);
//}
//
////////////////////////////////////////////////////////////////////////////////
////関数名	: SaveMotion::savePartsName
////返り値	: 
////説明		: 子供達の相対パスを保存
////////////////////////////////////////////////////////////////////////////////
//void SaveMotion::savePartsName(FILE * fp, const Parts * part)
//{
//	fprintf(fp, "PARTS_NAME %s\n", part->mModelData->getModelName().c_str());
//
//#pragma omp parallel for
//	for (auto it = part->mChildrenList.begin(); it != part->mChildrenList.end(); ++it)
//	{
//		auto child = *it;
//		savePartsName(fp, child);
//	}
//}
//
//void SaveMotion::savePartsPosture(FILE * fp, const Parts & part)
//{
//	fprintf(fp, "SET_MODEL %s\n{\n", part.mModelData->getModelName().c_str());
//	saveTransform(fp, part.mTransform);
//	fprintf(fp, "}\n");
//}
//
////////////////////////////////////////////////////////////////////////////////
////関数名	: SaveMotion::savePartsTransform
////返り値	: 
////説明		: 子供達を順に保存
////////////////////////////////////////////////////////////////////////////////
//void SaveMotion::savePartsInfomation(FILE * fp, const list<MotionData*> motionList)
//{
//	fprintf(fp, "#####################################################\n");
//	fprintf(fp, "# モーションの保存\n");
//	fprintf(fp, "#####################################################\n");
//	for (auto motionListIt = motionList.begin(); motionListIt != motionList.end(); ++motionListIt)
//	{
//		auto data = *motionListIt;
//		fprintf(fp, "MOTION %s\n{\n", data->mMotionName.c_str());
//		fprintf(fp, "\tplay\t= %d\n", data->mPlayMode);
//		
//		for (auto keyFrameIt = data->mKeyframeList.begin(); keyFrameIt != data->mKeyframeList.end(); ++keyFrameIt)
//		{
//			auto keyFrame = *keyFrameIt;
//			fprintf(fp, "SET_KEY_FRAME\n{\n");
//			fprintf(fp, "\tframe\t= %d\n", keyFrame->mFrameRate);
//
//			for (auto keyIt = keyFrame->mKey.begin(); keyIt != keyFrame->mKey.end(); ++keyIt)
//			{
//				auto key = *keyIt;
//				fprintf(fp, "KEY %s\n{\n", key.first.c_str());
//				saveTransform(fp, key.second);
//				fprintf(fp, "}\n");
//			}
//			fprintf(fp, "}\n");
//		}
//		fprintf(fp, "}\n");
//	}
//}

/*
#################################################################################################################################################
SaveCollision class
#################################################################################################################################################
*/
//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveCollision::saveFile
//返り値	: 
//説明		: 当たり判定の保存
//////////////////////////////////////////////////////////////////////////////
void SaveCollision::saveFile(string fileName, string saveMode)
{
	if (fileName == "\0")
		return;

	FILE	*fp = fopen(fileName.c_str(), saveMode.c_str());

	//ヘッダーコメント
	fprintf(fp, "#####################################################\n");
	fprintf(fp, "# コリジョン設定\n");
	fprintf(fp, "#####################################################\n");

	auto	ui = (CollisionEditorUi*)Manager::getUi();
	saveCollisionSceneHas(fp, ui->getObjectList());

	fclose(fp);
	MessageBox(NULL, "保存完了", "メッセージ", MB_OK);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveCollision::saveCollisionBox
//返り値	: 
//説明		: コリジョンボックス保存
//////////////////////////////////////////////////////////////////////////////
void SaveCollision::saveCollisionBox(FILE * fp, CollisionBox * col)
{
	auto	box = (CollisionBox*)col;
	fprintf(fp, "COLLISION_BOX\n");
	fprintf(fp, "{\n");
	saveTransform(fp, Transform(box->getPos(), box->getRot(), box->getSize()));
	fprintf(fp, "}\n");
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveCollision::saveCollisionBall
//返り値	: 
//説明		: コリジョンボール保存
//////////////////////////////////////////////////////////////////////////////
void SaveCollision::saveCollisionBall(FILE * fp, CollisionBall * col)
{
	auto	ball = (CollisionBall*)col;
	fprintf(fp, "COLLISION_BALL\n");
	fprintf(fp, "{\n");
	fprintf(fp, "\tpos\t\t= %f %f %f\n", ball->getPos().x, ball->getPos().y, ball->getPos().z);
	fprintf(fp, "\tlen\t\t= %f\n", ball->getLen());
	fprintf(fp, "}\n");
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveCollision::saveEachType
//返り値	: 
//説明		: タイプごと保存
//////////////////////////////////////////////////////////////////////////////
void SaveCollision::saveEachType(FILE * fp, Collision::TYPE type, Collision * col)
{
	switch (type)
	{
	case COLLISION_BOX:
		saveCollisionBox(fp, (CollisionBox*)col);
		break;

	case COLLISION_BALL:
		saveCollisionBall(fp, (CollisionBall*)col);
		break;
	}
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveCollision::saveCollisionList
//返り値	: 
//説明		: コリジョンリスト保存
//////////////////////////////////////////////////////////////////////////////
void SaveCollision::saveCollisionList(FILE * fp, const list<Collision*> colList)
{
#pragma omp parallel for
	for (auto colIt = colList.begin(); colIt != colList.end(); ++colIt)
	{
		auto	col = *colIt;
		saveEachType(fp, col->getType(), col);
	}
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveCollision::saveCollisionSceneHas
//返り値	: 
//説明		: シーンが持っているコリジョン保存
//////////////////////////////////////////////////////////////////////////////
void SaveCollision::saveCollisionSceneHas(FILE * fp, list<Scene*> sceneList)
{
#pragma omp parallel for
	for (auto sceneIt = sceneList.begin(); sceneIt != sceneList.end(); ++sceneIt)
	{
		auto	scene = *sceneIt;
		auto	colManager = scene->getCollision();
		if (colManager == NULL)
			continue;

		saveCollisionList(fp, *colManager->getCollisionList());
	}
}

/*
#################################################################################################################################################
SaveModel class
#################################################################################################################################################
*/
//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveModel::saveFile
//返り値	: 
//説明		: モデル情報保存
//////////////////////////////////////////////////////////////////////////////
void SaveModel::saveFile(string fileName)
{
	if (fileName == "\0")
		return;

	FILE	*fp = fopen(fileName.c_str(), "w");
	auto	modelList = Scene::getTypeList(Scene::OBJECT);
	
	//モデル名リスト保存
	saveModelName(fp, modelList);

	//モデル情報保存
	saveModelData(fp, modelList);

	fclose(fp);
	MessageBox(NULL, "保存完了", "メッセージ", MB_OK);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveModel::saveModelName
//返り値	: 
//説明		: モデル名保存
//////////////////////////////////////////////////////////////////////////////
void SaveModel::saveModelName(FILE* fp, list<Scene*> sceneList)
{
	//ヘッダーコメント
	fprintf(fp, "#####################################################\n");
	fprintf(fp, "# モデルリスト\n");
	fprintf(fp, "#####################################################\n");

#pragma omp parallel for
	for (auto it = sceneList.begin(); it != sceneList.end(); ++it)
	{
		auto scene = *it;
		fprintf(fp, "PARTS_NAME %s\n", scene->getMyName().c_str());
	}

}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SaveModel::saveModelData
//返り値	: 
//説明		: モデル座標系保存
//////////////////////////////////////////////////////////////////////////////
void SaveModel::saveModelData(FILE * fp, list<Scene*> sceneList)
{
	fprintf(fp, "#####################################################\n");
	fprintf(fp, "# モデル情報\n");
	fprintf(fp, "#####################################################\n");

#pragma omp parallel for
	for (auto it = sceneList.begin(); it != sceneList.end(); ++it)
	{
		auto scene = *it;
		fprintf(fp, "SET_MODEL %s\n{\n", scene->getMyName().c_str());
		if (scene->getParent() != NULL)
			fprintf(fp, "\tparent\t= %s\n", scene->getParent()->getMyName().c_str());
		else
			fprintf(fp, "\tparent\t= NULL\n");

		saveTransform(fp, scene->getTransform());
		saveCollision(fp, scene);
		fprintf(fp, "}\n");
	}
}
