/*****************************************************************************
shelf.h
Aythor	: 上野　匠
Data	: 2017_08_01
=============================================================================
Updata

*****************************************************************************/
#ifndef SHELF_H
#define SHELF_H
//////////////////////////////////////////////////////////////////////////////
//ヘッダーファイルインクルード
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//前方宣言
//////////////////////////////////////////////////////////////////////////////
class Scene;
class SceneModel;

//////////////////////////////////////////////////////////////////////////////
//Shelfクラス
//////////////////////////////////////////////////////////////////////////////
class Shelf : public SceneModel
{
public:
	Shelf(){}
	~Shelf(){}

	HRESULT	init(void);
	void	uninit(void);
	void	update(void);
	void	draw(void);

	static Shelf	*create(void);

	Dvec3	getPos(void){return mTransform.pos;}
	Dvec3	getSize(void){return mTransform.size;}

	Dvec3	*getPosAd(void){return &mTransform.pos;}
	Dvec3	*getSizeAd(void){return &mTransform.size;}
	Dvec3	*getRotAd(void){return &mTransform.rot;}

private:
	
};

#endif // !SHELF_H