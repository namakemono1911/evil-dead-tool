/*****************************************************************************
bullet.h
Aythor	: 上野　匠
Data	: 2018_01_09
=============================================================================
Updata

*****************************************************************************/
#ifndef BULLET_H
#define	BULLET_H
//////////////////////////////////////////////////////////////////////////////
//前方宣言
//////////////////////////////////////////////////////////////////////////////
class Scene;
class Weapon;

//////////////////////////////////////////////////////////////////////////////
//クラス宣言
//////////////////////////////////////////////////////////////////////////////
class Bullet
{
public:
	Bullet() {}
	~Bullet() {}

	static Bullet*	create(Dvec3 pos, Dvec3 moveVec, float bulletSpeed, float range);
	
	void	update();
	Scene*	getHittingByEnemy();
	Dvec3&	pos() { return mPos; }
	Dvec3&	moveVec() { return mMoveVec; }
	float&	bulletSpeed() { return mBulletSpeed; }
	auto&	isClear() { return mIsClear; }

private:
	bool	lengthComparison(float &shortDistance, const float &len);
	bool	hitToCollisionBox(CollisionBox* box, float &shortDistance);
	bool	hitToCollisionBall(CollisionBall* ball, float &shortDistance);
	bool	hitToCollision(Collision *col, float &shortDistance);
	bool	searchHitCollision(CollisionManager *colManager, float &shortDistance);
	Scene*	searchHitScene(list<Scene*> sceneList, float &shortDistance);

	Dvec3	mStartPos;			//発射された位置
	Dvec3	mPos;				//位置
	Dvec3	mMoveVec;			//方向ベクトル
	float	mBulletSpeed;		//弾速
	float	mEffectiveRange;	//有効射程
	bool	mIsClear;			//削除フラグ

};

#endif // !BULLET_H
