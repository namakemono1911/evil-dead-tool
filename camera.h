/*****************************************************************************
camera.h
Aythor	: 上野　匠
Data	: 2017_04_20
=============================================================================
Updata

*****************************************************************************/
#ifndef CAMERA_H
#define CAMERA_H
//////////////////////////////////////////////////////////////////////////////
//定数定義
//////////////////////////////////////////////////////////////////////////////
#define MAX_CAMERA	(4)

//////////////////////////////////////////////////////////////////////////////
//クラス定義
//////////////////////////////////////////////////////////////////////////////
class Camera
{
public:
	Camera();
	~Camera();
	HRESULT		init(D3DXVECTOR3 pos, D3DXVECTOR3 at, D3DXVECTOR3 up, float len, int id);
	void		update(void);
	static void	updateAll(void);

	static Camera	*create(D3DXVECTOR3 pos, D3DXVECTOR3 at, D3DXVECTOR3 up, float len);
	void			release(void);

	void	setPosEye(vec3 pos){mPosEye = vec3::conv(pos);}
	void	setPosAt(vec3 pos){mPosAt = vec3::conv(pos);}
	void	setAngle(vec2 angle){mAngle = vec2::conv(angle);}
	void	setAspect(float as)
	{
		mAspect = as;
		setCamera();
	}
	void	rollAt(vec3 angle) 
	{
		mPosAt = D3DXVECTOR3(
			mLen * angle.x + mPosEye.x,
			mLen * angle.y + mPosEye.y,
			mLen * angle.z + mPosEye.z);
	}
	void	moveEye(vec3 vec, float move)
	{
		mPosEye += D3DXVECTOR3(
			move * vec.x,
			move * vec.y,
			move * vec.z);
		mPosAt += D3DXVECTOR3(
			move * vec.x,
			move * vec.y,
			move * vec.z);
	}
	void setViewMtx(D3DXMATRIX mtx){mView = mtx;}
	void setFovy(float fovy){mFovy = fovy; setCamera();}
	void setLen(float len) { mLen = len; }

	D3DXMATRIX	getViewMtx(void);
	D3DXVECTOR3	getEye(void) {return mPosEye;}
	D3DXVECTOR3	getAt(void) {return mPosAt;}
	D3DXVECTOR2 getAngle(void) {return mAngle;}
	float		getLen(void) {return mLen;}
	float		getAspect(void) {return mAspect;}
	float		getFovy(void){return mFovy;}
	auto		getAtAd(void) { return &mPosAt; }
	auto		getEyeAd(void) { return &mPosEye; }
	auto		getAngleAd(void) { return &mAngle; }
	auto		getLenAd(void) { return &mLen; }

private:
	//プロトタイプ宣言
	void	setCamera(void);

	static Camera		*mCamera[MAX_CAMERA];	//自身のポインタ
	D3DXMATRIX	mView;		//カメラ行列
	D3DXVECTOR3	mPosAt;		//注視点
	D3DXVECTOR3	mPosEye;	//カメラの座標
	D3DXVECTOR3	mVecUp;		//カメラの方向
	D3DXMATRIX	mProj;		//プロジェクション行列
	D3DXVECTOR2	mAngle;		//カメラの角度
	float		mLen;		//中心からの距離
	float		mAspect;	//アスペクト比
	float		mFovy;		//視野角
	int			mID;		//自分の配列番号
};

#endif