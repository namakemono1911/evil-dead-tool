/*****************************************************************************
matrix.cpp
Aythor	: 上野　匠
Data	: 2017_08_03
=============================================================================
Updata

*****************************************************************************/
//////////////////////////////////////////////////////////////////////////////
//ファイルインクルード
//////////////////////////////////////////////////////////////////////////////
#include "main.h"
#include "matrix.h"

//////////////////////////////////////////////////////////////////////////////
//関数名	: Mtx::screenToWorld()
//返り値	: 
//説明		: ワールド座標からスクリーン座標に変換
//////////////////////////////////////////////////////////////////////////////
D3DXVECTOR3 * Mtx::screenToWorld(D3DXVECTOR3 * pout, int Sx, int Sy, float fZ, int Screen_w, int Screen_h, D3DXMATRIX * View, D3DXMATRIX * Prj)
{
	// 各行列の逆行列を算出
	D3DXMATRIX InvView, InvPrj, VP, InvViewport;
	D3DXMatrixInverse(&InvView, NULL, View);
	D3DXMatrixInverse(&InvPrj, NULL, Prj);
	D3DXMatrixIdentity(&VP);
	VP._11 = Screen_w / 2.0f; VP._22 = -Screen_h / 2.0f;
	VP._41 = Screen_w / 2.0f; VP._42 = Screen_h / 2.0f;
	D3DXMatrixInverse(&InvViewport, NULL, &VP);

	// 逆変換
	D3DXMATRIX tmp = InvViewport * InvPrj * InvView;
	D3DXVec3TransformCoord(pout, &D3DXVECTOR3((float)Sx, (float)Sy, fZ), &tmp);

	return pout;
}
