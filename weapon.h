/*****************************************************************************
weapon.cpp
Aythor	: 上野　匠
Data	: 2018_01_09
=============================================================================
Updata

*****************************************************************************/
#ifndef WEAPON_H
#define WEAPON_H

#include "sceneModel.h"

//////////////////////////////////////////////////////////////////////////////
//前方宣言
//////////////////////////////////////////////////////////////////////////////
class Scene;
class SceneModel;
class Bullet;
class ModelParts;

//////////////////////////////////////////////////////////////////////////////
//クラス定義
//////////////////////////////////////////////////////////////////////////////
class Weapon : public SceneModel
{
public:
	Weapon() {};
	~Weapon() {};

	HRESULT	init() override;
	void	uninit() override;
	void	update() override;
	void	draw() override;

	virtual void	shot() = 0;
	virtual void	reload() = 0;
	virtual void	aim() = 0;
	virtual void	moving() = 0;
	virtual void	neutral() = 0;

protected:
	list<Bullet*>	mBulletList;
	int		mMagazineCapacity;		//マガジン内に入る弾数
	int		mBullet;				//残弾
	int		mBulletInMagazine;		//マガジン内の残弾
};

class NullWeapon : public Weapon
{
public:
	NullWeapon() {};
	~NullWeapon() {};

	void	shot() override {}
	void	reload() override {}
	void	aim() override {}
	void	moving() override {}
	void	neutral() override {}

private:

};

class Goverment : public Weapon
{
public:
	Goverment() {};
	Goverment(Dvec3 pos) { mTransform.pos = pos; }
	~Goverment() {};

	HRESULT	init() override;
	void	uninit() override;
	void	update() override;
	void	draw() override { mModelParts.drawAllParts(); }

	void	shot() override;
	void	reload() override;
	void	aim() override;
	void	moving() override;
	void	neutral() override;

	static Goverment*	create(Dvec3 pos);

private:
	ModelParts	mModelParts;	//モデルパーツのデータ
};

#endif // !WEAPON_H