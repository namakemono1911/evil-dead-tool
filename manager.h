/*****************************************************************************
manager.h
Aythor	: 上野　匠
Data	: 2017_05_11
=============================================================================
Updata

*****************************************************************************/
#ifndef MANAGER_H
#define MANAGER_H
//////////////////////////////////////////////////////////////////////////////
//インクルード
//////////////////////////////////////////////////////////////////////////////
#include "input.h"
#include "scene.h"
#include "scene2D.h"
#include "scene3D.h"
#include "sceneModel.h"
#include "sceneGrit.h"
#include "ui.h"
#include "mesh.h"
#include "camera.h"
#include "player.h"
#include "save.h"

//////////////////////////////////////////////////////////////////////////////
//定数定義
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//前方宣言
//////////////////////////////////////////////////////////////////////////////
class Renderer;
class Input;
class InputKeyboard;
class InputMouse;
class Player;
class Mode;
class Ui;
class Camera;

//////////////////////////////////////////////////////////////////////////////
//クラス宣言
//////////////////////////////////////////////////////////////////////////////
class Manager
{
public:
	Manager(){}
	~Manager(){}

	HRESULT	init	(HINSTANCE hInstance, HWND hWnd, BOOL bWindow);
	void	uninit	(void);
	void	update	(void);
	void	draw	(void);

	//ゲッター
	static Renderer				*getRenderer(void){return mRenderer;}
	static LPDIRECT3DDEVICE9	getDevice(void){return mRenderer->getDevice();}
	static InputKeyboard		*getKeyboard(void){return mKey;}
	static InputMouse			*getMouse(void){return mMouse;}
	static Ui					*getUi(void){return mUi;}
	static Camera				*getCamera(void){return mCamera;}
	static Player				*getPlayer(void){return mPlayer;}
	static Stage				*getStage(void) { return mStage; }

	static void					reset(void);
	static void					setStage(string fileName);

private:
	static Renderer				*mRenderer;		//レンダラー
	static InputKeyboard		*mKey;			//キーボード
	static InputMouse			*mMouse;		//マウス
	static Ui					*mUi;			//uiのやつ
	static Camera				*mCamera;		//カメラ
	static Player				*mPlayer;		//プレイヤー
	static Stage				*mStage;		//ステージ
};

#endif