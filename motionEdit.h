/*****************************************************************************
motion.h
Aythor	: 上野　匠
Data	: 2017_09_06
=============================================================================
Updata

*****************************************************************************/
#ifndef MOTION_EDIT_H
#define MOTION_EDIT_H
//////////////////////////////////////////////////////////////////////////////
// ヘッダーインクルード
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// クラス宣言
//////////////////////////////////////////////////////////////////////////////
class Scene;
class SceneModel;
class ModelParts;

class MotionEdit : public SceneModel
{
public:
	MotionEdit() {}
	~MotionEdit() {}

	HRESULT		init(void);
	void		uninit(void);
	void		update(void);
	void		draw(void);

	/*ModelParts	getParts(void) { return mParts; }
	auto		getPartsAddr(void) { return &mParts; }
	int			getnumParts(void) { return sizeof(mPartsList) / sizeof(Parts*); }

	ModelParts	*getModelPartsAddr(void) { return &mParts; }*/

	static MotionEdit	*create(Transform transform);
	static MotionEdit	*create(string fileDir);

private:
	list<SceneModel*>	mSceneList;
	/*Parts				*mPartsList;
	ModelParts			mParts;*/
};

#endif // !HUMAN_H