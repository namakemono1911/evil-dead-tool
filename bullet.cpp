/*****************************************************************************
bullet.cpp
Aythor	: 上野　匠
Data	: 2018_01_09
=============================================================================
Updata

*****************************************************************************/
//////////////////////////////////////////////////////////////////////////////
//ヘッダーインクルード
//////////////////////////////////////////////////////////////////////////////
#include <list>
#include "main.h"
#include "scene.h"
#include "weapon.h"
#include "collision.h"
#include "collisionBox.h"
#include "collisionBall.h"
#include "bullet.h"

Bullet * Bullet::create(Dvec3 pos, Dvec3 moveVec, float bulletSpeed, float range)
{
	auto bullet = new Bullet;
	bullet->mStartPos = pos;
	bullet->mPos = pos;
	bullet->mMoveVec = moveVec;
	bullet->mBulletSpeed = bulletSpeed;
	bullet->mEffectiveRange = range;
	bullet->mIsClear = false;
	return bullet;
}

void Bullet::update()
{
	mPos += mMoveVec * mBulletSpeed;

	if (D3DXVec3Length(&Dvec3(mPos - mStartPos)) > mEffectiveRange)
		mIsClear = true;
}

Scene* Bullet::getHittingByEnemy()
{
	Scene*	nearScene = NULL;
	float shortDistance = D3DXVec3Length(&Dvec3(mMoveVec * mBulletSpeed));
	nearScene = searchHitScene(Scene::getTypeList(Scene::OBJECT), shortDistance);
	nearScene = searchHitScene(Scene::getTypeList(Scene::ZOMBIE), shortDistance);

	return nearScene;
}

bool Bullet::lengthComparison(float & shortDistance, const float & len)
{
	if (shortDistance > len)
	{
		shortDistance = len;
		return true;
	}
	return false;
}

bool Bullet::hitToCollisionBox(CollisionBox * box, float & shortDistance)
{
	Dvec3 crossPos;
	Dvec3 axis;
	if (box->hitByRay(&crossPos, &axis, mPos, mMoveVec * mBulletSpeed + mPos))
	{
		mIsClear = true;
		return lengthComparison(shortDistance, D3DXVec3Length(&Dvec3(crossPos - mPos)));
	}
	return false;
}

bool Bullet::hitToCollisionBall(CollisionBall * ball, float & shortDistance)
{
	Dvec3 intersection;
	if (ball->hitByRay(intersection, mPos, mMoveVec * mBulletSpeed) == true)
	{
		mIsClear = true;
		return lengthComparison(shortDistance, D3DXVec3Length(&intersection));
	}
	return false;
}

bool Bullet::hitToCollision(Collision * col, float & shortDistance)
{
	switch (col->getType())
	{
	case Collision::BOX:
		return hitToCollisionBox((CollisionBox*)col, shortDistance);
		break;

	case Collision::BALL:
		return hitToCollisionBall((CollisionBall*)col, shortDistance);
		break;
	}
	return false;
}

bool Bullet::searchHitCollision(CollisionManager *colManager, float & shortDistance)
{
	if (colManager == NULL)
		return false;

	for (auto colIt = colManager->getCollisionList()->begin(); colIt != colManager->getCollisionList()->end(); ++colIt)
		return hitToCollision(*colIt, shortDistance);
}

Scene * Bullet::searchHitScene(list<Scene*> sceneList, float & shortDistance)
{
	Scene* nearScene = NULL;
	for (auto sceneIt = sceneList.begin(); sceneIt != sceneList.end(); ++sceneIt)
	{
		auto scene = *sceneIt;
		if (searchHitCollision(scene->getCollision, shortDistance))
			nearScene = scene;
	}
	return nearScene;
}
