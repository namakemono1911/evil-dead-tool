/*****************************************************************************
manager.cpp
Aythor	: 上野　匠
Data	: 2017_05_11
=============================================================================
Updata

*****************************************************************************/
//////////////////////////////////////////////////////////////////////////////
//ヘッダーファイルインクルード
//////////////////////////////////////////////////////////////////////////////
#include "main.h"
#include "renderer.h"
#include "manager.h"
#include "meshField.h"
#include "light.h"
#include "collisionBall.h"
#include "human.h"
#include "stage.h"

//////////////////////////////////////////////////////////////////////////////
//静的メンバ変数初期化
//////////////////////////////////////////////////////////////////////////////
Renderer		*Manager::mRenderer = NULL;
InputKeyboard	*Manager::mKey = NULL;
InputMouse		*Manager::mMouse = NULL;
Ui				*Manager::mUi = NULL;
Camera			*Manager::mCamera = NULL;
Player			*Manager::mPlayer = NULL;
Stage			*Manager::mStage = NULL;

//////////////////////////////////////////////////////////////////////////////
//関数名	: init
//返り値	: 
//説明		: 初期化処理
//////////////////////////////////////////////////////////////////////////////
HRESULT	Manager::init	(HINSTANCE hInstance, HWND hWnd, BOOL bWindow)
{
	//レンダラー生成
	mRenderer = new Renderer;
	mRenderer->init(hWnd, bWindow);

	////キーボード設定
	mKey = new InputKeyboard;
	mKey->init(hInstance, hWnd);

	////マウス設定
	mMouse = new InputMouse;
	mMouse->init(hInstance, hWnd);
	//mMouse->fixing(true);				//マウス固定
	//ShowCursor(false);				//マウス非表示

	//カメラ生成
	mCamera = Camera::create(
		D3DXVECTOR3(0.f, 10.f, -30.f),
		D3DXVECTOR3(0.f, 0.f, 0.f),
		D3DXVECTOR3(0.f, 1.f, 0.f),
		-30);

	//ライト生成
	/*Light::setLight(
	D3DXVECTOR3(0.f, 10.f, 0.f),
	D3DXVECTOR3(0.f, 0.f, 0.f),
	D3DCOLOR_RGBA(0, 0, 0, 0),
	D3DCOLOR_RGBA(255, 255, 255, 192));*/

	Light::setLight(
		D3DXVECTOR3(0.f, 10.f, 10.f),
		D3DXVECTOR3(0.f, -0.5f, -1.f),
		D3DCOLOR_RGBA(192, 192, 192, 255),
		D3DCOLOR_RGBA(255, 255, 255, 255));

	//全モデル読み込み
	ModelData::loadAllModel();

	//コリジョンテクスチャ読み込み
	CollisionBall::loadTex();

	//フィールド
	MeshField::loadTex();

#ifdef MAP_EDITOR
	mStage = Stage::create();
	mUi = new MapEditorUi;
#endif // MAP_EDITOR

#ifdef MOTION_EDITOR
	//UI設定
	mUi = new MotionEditorUi;
#endif // MOTION_EDITOR

#ifdef COLLISION_EDITOR
	mUi = new CollisionEditorUi;
#endif // COLLISION_EDITOR

#ifdef MODEL_EDITOR
	mUi = new ModelEditorUi;
#endif // MODEL_EDITOR

	mUi->init();

	return S_OK;
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: uninit
//返り値	: 
//説明		: 初期化処理
//////////////////////////////////////////////////////////////////////////////
void	Manager::uninit	( void )
{
	//レンダラー終了
	mRenderer->uninit();
	delete mRenderer;
	mRenderer = NULL;

	//ライト終了
	Light::releaseAll();

	//キーボード終了
	mKey->uninit();
	delete mKey;

	//マウス終了
	mMouse->uninit();
	delete mMouse;

	//UI終了
	mUi->uninit();
	delete mUi;

	//モデル情報破棄
	ModelData::modelDataAllClear();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: update
//返り値	: 
//説明		: 初期化処理
//////////////////////////////////////////////////////////////////////////////
void	Manager::update	( void )
{
	//キーボード更新
	mKey->update();

	//マウス
	mMouse->update();

	//オブジェクト更新
	Scene::updateAll();

	//カメラ更新
	mCamera->update();

	//UI更新
	mUi->update();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: draw
//返り値	: 
//説明		: 初期化処理
//////////////////////////////////////////////////////////////////////////////
void	Manager::draw	( void )
{
	//描画
	mRenderer->draw();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: reset
//返り値	: 
//説明		: リセット
//////////////////////////////////////////////////////////////////////////////
void Manager::reset(void)
{
	mUi->uninit();
	Scene::uninitAll();
	mStage = NULL;
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: setStage
//返り値	: 
//説明		: ステージ設置
//////////////////////////////////////////////////////////////////////////////
void Manager::setStage(string fileName)
{
	mStage = Stage::create();
	mStage->loadMapFile(fileName);
}
