/*****************************************************************************
camera.cpp
Aythor	: 上野　匠
Data	: 2017_04_20
=============================================================================
Updata

*****************************************************************************/
//////////////////////////////////////////////////////////////////////////////
//ファイルインクルード
//////////////////////////////////////////////////////////////////////////////
#include "main.h"
#include "input.h"
#include "renderer.h"
#include "camera.h"
#include "manager.h"

//////////////////////////////////////////////////////////////////////////////
//静的メンバ変数初期化
//////////////////////////////////////////////////////////////////////////////
Camera *Camera::mCamera[MAX_CAMERA] = {NULL};

//////////////////////////////////////////////////////////////////////////////
//関数名	: Camera::Camera()
//返り値	: 
//説明		: コンストラクタ
//////////////////////////////////////////////////////////////////////////////
Camera::Camera()
{

}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Camera::~Camera()
//返り値	: 
//説明		: デストラクタ
//////////////////////////////////////////////////////////////////////////////
Camera::~Camera()
{

}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Camera::init
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
HRESULT	Camera::init(D3DXVECTOR3 pos, D3DXVECTOR3 at, D3DXVECTOR3 up, float len, int id)
{
	mVecUp = up;
	mAngle = D3DXVECTOR2(0.f, 0.f);
	mLen = len;
	mAspect = (float)(SCREEN_WIDTH / SCREEN_HEIGHT);
	mFovy = PI/ 3.f;

	mPosEye = pos;
	mPosAt = Dvec3(
		len * cosf(mAngle.y) * sinf(mAngle.x + PI) + pos.x,
		len * sinf(mAngle.y) + pos.y,
		len * cosf(mAngle.y) * cosf(mAngle.x + PI) + pos.z);

	mID = id;

	return S_OK;
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Camera::update
//返り値	: 
//説明		: 更新
//////////////////////////////////////////////////////////////////////////////
void	Camera::update(void)
{
	setCamera();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Camera::updateAll
//返り値	: 
//説明		: 全更新
//////////////////////////////////////////////////////////////////////////////
void	Camera::updateAll(void)
{
	for(int i = 0; i < MAX_CAMERA; i++)
	{
		if(mCamera[i] != NULL)
		{
			mCamera[i]->update();
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Camera::create
//返り値	: 
//説明		: カメラ生成
//////////////////////////////////////////////////////////////////////////////
Camera		*Camera::create(D3DXVECTOR3 pos, D3DXVECTOR3 at, D3DXVECTOR3 up, float len)
{
	//未使用の配列探索
	for(int i = 0; i < MAX_CAMERA; i++)
	{
		if(mCamera[i] == NULL)
		{
			//生成
			mCamera[i] = new Camera;

			//初期化
			mCamera[i]->init(pos, at, up, len, i);

			return mCamera[i];
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Camera::release
//返り値	: 
//説明		: カメラ生成
//////////////////////////////////////////////////////////////////////////////
void	Camera::release(void)
{
	//変数宣言
	int	idWork = mID;		//ID一時保存

	delete mCamera[idWork];
	mCamera[idWork] = NULL;
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Camera::getViewMtx
//返り値	: 
//説明		: ビューマトリックス取得
//////////////////////////////////////////////////////////////////////////////
D3DXMATRIX	Camera::getViewMtx(void)
{
	return mView;
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Camera::setCamera
//返り値	: 
//説明		: ビューマトリックス設定
//////////////////////////////////////////////////////////////////////////////
void Camera::setCamera(void)
{
	//変数宣言
	LPDIRECT3DDEVICE9	device = Renderer::getDevice();

	//カメラ行列の設定
	D3DXMatrixLookAtLH(&mView, &mPosEye, &mPosAt, &mVecUp);
	device->SetTransform(D3DTS_VIEW, &mView);

	//プロジェクション行列の設定
	D3DXMatrixPerspectiveFovLH(&mProj,
		mFovy,				//視野角
		mAspect,			//アスペクト比
		1.f,				//near
		1000.f);			//far

	device->SetTransform(D3DTS_PROJECTION, &mProj);	//行列設定
}
