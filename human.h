/*****************************************************************************
motion.h
Aythor	: 上野　匠
Data	: 2017_09_06
=============================================================================
Updata

*****************************************************************************/
#ifndef HUMAN_H
#define HUMAN_H

#include "scene.h"
#include "sceneModel.h"
#include "motion.h"

//////////////////////////////////////////////////////////////////////////////
// ヘッダーインクルード
//////////////////////////////////////////////////////////////////////////////
class Human : public SceneModel
{
public:
	Human(){}
	~Human(){}

	HRESULT		init	(void);
	void		uninit	(void);
	void		update	(void);
	void		draw	(void);
	Dvec3		getPos	(void){return mTransform.pos;}
	Dvec3		getSize	(void){return mTransform.size;}
	Dvec3		getRot	(void){return mTransform.rot;}
	//ModelParts	getParts(void){return mParts;}
	//auto		getPartsAddr(void){return &mParts;}
	//int			getnumParts(void){return sizeof(mPartsList) / sizeof(Parts*);}

	//ModelParts	*getModelPartsAddr(void){return &mParts;}

	static Human	*create(Transform transform);

private:
	list<SceneModel*>	mSceneList;
	//Parts				*mPartsList;
	//ModelParts			mParts;
};

#endif // !HUMAN_H