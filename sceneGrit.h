/*****************************************************************************
sceneGrit.h
Aythor	: 上野　匠
Data	: 2017_07_26
=============================================================================
Updata

*****************************************************************************/
#ifndef SCENE_GRIT_H
#define	SCENE_GRIT_H
//////////////////////////////////////////////////////////////////////////////
//SceneGrit class
//////////////////////////////////////////////////////////////////////////////
class SceneGrit : public Scene
{
public:
	SceneGrit(){}
	~SceneGrit(){}

	HRESULT	init(void);
	void	uninit(void);
	void	update(void);
	void	draw(void);

	Dvec3	getPos(void){return Dvec3();}
	Dvec3	getSize(void){return Dvec3();}
	Dvec3	getRot(void){return Dvec3();}
	Dvec3	*getPosAd(void){return nullptr;}
	Dvec3	*getSizeAd(void){return nullptr;}
	Dvec3	*getRotAd(void){return nullptr;}

	static SceneGrit *create(void);

private:
	LPDIRECT3DVERTEXBUFFER9		mVtx;	//頂点バッファ

};

#endif // !SCENE_GRIT_H
