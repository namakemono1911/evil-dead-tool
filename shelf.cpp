/*****************************************************************************
shelf.cpp
Aythor	: 上野　匠
Data	: 2017_08_01
=============================================================================
Updata

*****************************************************************************/
//////////////////////////////////////////////////////////////////////////////
//ファイルインクルード
//////////////////////////////////////////////////////////////////////////////
#include "main.h"
#include "manager.h"
#include "shelf.h"

//////////////////////////////////////////////////////////////////////////////
//関数名	: Shelf::init()
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
HRESULT Shelf::init(void)
{
	if (FAILED(loadModel("data/model/shelf.x")))
		return E_FAIL;

	mTransform.pos = Dvec3(0, 0, 0);
	mTransform.size = Dvec3(1, 1, 1);

	SceneModel::init();

	return S_OK;
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Shelf::init()
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
void Shelf::uninit(void)
{
	SceneModel::uninit();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Shelf::init()
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
void Shelf::update(void)
{
	SceneModel::update();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Shelf::init()
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
void Shelf::draw(void)
{
	SceneModel::draw();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Shelf::create()
//返り値	: 
//説明		: 生成
//////////////////////////////////////////////////////////////////////////////
Shelf * Shelf::create(void)
{
	Shelf *shelf = new Shelf;

	shelf->init();

	return shelf;
}
