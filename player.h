/*****************************************************************************
player.h
Aythor	: 上野　匠
Data	: 2017_06_15
=============================================================================
Updata

*****************************************************************************/
#ifndef PLAYER_H
#define PLAYER_H
//////////////////////////////////////////////////////////////////////////////
//前方宣言
//////////////////////////////////////////////////////////////////////////////
class Scene;
class Camera;
class Fade;

//////////////////////////////////////////////////////////////////////////////
//プレイヤークラス
//////////////////////////////////////////////////////////////////////////////
class Player : public Scene
{
public:
	Player(UINT priolity = PRIME_FIRST, OBJ_TYPE type = PLAYER) :Scene(priolity) { setObjType(type); }
	~Player(){}

	HRESULT	init() { return S_OK; };
	void	uninit() {}
	void	update() {}
	void	draw() {}

	static Player	*create(Player *player);

private:

};

//////////////////////////////////////////////////////////////////////////////
//プレイヤーエディットクラス
//////////////////////////////////////////////////////////////////////////////
class PlayerEdit : public Player
{
public:
	PlayerEdit(){}
	~PlayerEdit(){}

	HRESULT	init(void);
	void	uninit(void);
	void	update(void);

private:
	float	mSens;
};

#endif // !PLAYER_H
