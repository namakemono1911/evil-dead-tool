/*****************************************************************************
sceneGrit.cpp
Aythor	: 上野　匠
Data	: 2017_07_26
=============================================================================
Updata

*****************************************************************************/
//////////////////////////////////////////////////////////////////////////////
//ファイルインクルード
//////////////////////////////////////////////////////////////////////////////
#include "main.h"
#include "manager.h"
#include "sceneGrit.h"

//////////////////////////////////////////////////////////////////////////////
//定数定義
//////////////////////////////////////////////////////////////////////////////
#define NUM_GRIT	(11)		//グリット数

//////////////////////////////////////////////////////////////////////////////
//関数名	: SceneGrit::init
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
HRESULT SceneGrit::init(void)
{

	//変数宣言
	LPDIRECT3DDEVICE9	device;	//デバイス情報
	VERTEX2D	*pVtx = NULL;	//仮想アドレス用ポインタ

	//デバイス情報取得
	device = Renderer::getDevice();

	//頂点バッファの設定
	if (FAILED(device->CreateVertexBuffer
	(
		sizeof(VERTEX2D) * NUM_GRIT * 2 * 2,	//頂点バッファサイズ
		D3DUSAGE_WRITEONLY,				//頂点バッファの使用方法
		FVF_VERTEX_2D,					//登録設定
		D3DPOOL_MANAGED,				//メモリ管理方法
		&mVtx,							//頂点バッファ管理インターフェイス
		NULL
	)
	))
	{
		return E_FAIL;
	}

	//バッファ登録開始
	mVtx->Lock(0, 0, (void**)&pVtx, 0);

	Dvec3 point((int)(NUM_GRIT / 2) * 10.f, 0.f, (int)(NUM_GRIT / 2) * 10.f);
	for (int x = 0; x < NUM_GRIT; x++)
	{
		pVtx[0].pos = Dvec3(point.x - 10.f * x, 0.f, point.z);
		pVtx[0].color = Dcolor(0.5f, 0.5f, 0.5f, 1.f);
		pVtx[1].pos = Dvec3(point.x - 10.f * x, 0.f, -point.z);
		pVtx[1].color = Dcolor(0.5f, 0.5f, 0.5f, 1.f);
		pVtx += 2;
	}
	for (int z = 0; z < NUM_GRIT; z++)
	{
		pVtx[0].pos = Dvec3(point.x, 0.f, point.z - 10.f * z);
		pVtx[0].color = Dcolor(0.5f, 0.5f, 0.5f, 1.f);
		pVtx[1].pos = Dvec3(-point.x, 0.f, point.z - 10.f * z);
		pVtx[1].color = Dcolor(0.5f, 0.5f, 0.5f, 1.f);
		pVtx += 2;
	}

	//登録終了
	mVtx->Unlock();

	return S_OK;
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SceneGrit::uninit
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
void SceneGrit::uninit(void)
{
	if (mVtx != NULL)
	{
		mVtx->Release();
		mVtx = NULL;
	}

	release();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SceneGrit::update
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
void SceneGrit::update(void)
{
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SceneGrit::draw
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
void SceneGrit::draw(void)
{
	//変数宣言
	LPDIRECT3DDEVICE9	device;		//デバイス情報

									//デバイス情報取得
	device = Renderer::getDevice();

	//ストリーム作成
	device->SetStreamSource(0, mVtx, 0, sizeof(VERTEX3D));

	//頂点フォーマットの設定
	device->SetFVF(FVF_VERTEX_3D);

	//テクスチャの設定
	device->SetTexture(0, NULL);

	//ライトの影響off
	Manager::getRenderer()->getDevice()->SetRenderState(D3DRS_LIGHTING, false);

	//ポリゴンの描画
	device->DrawPrimitive
	(
		D3DPT_LINELIST,			//ポリゴンの種類
		0,						//オフセット(頂点数)
		NUM_GRIT				//ポリゴンの数
	);

	//ライトの影響off
	Manager::getRenderer()->getDevice()->SetRenderState(D3DRS_LIGHTING, true);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: SceneGrit::create
//返り値	: 
//説明		: 生成
//////////////////////////////////////////////////////////////////////////////
SceneGrit * SceneGrit::create(void)
{
	SceneGrit *grit = new SceneGrit;

	grit->init();

	return grit;
}
