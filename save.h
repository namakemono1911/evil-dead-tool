/*****************************************************************************
save.h
Aythor	: 上野　匠
Data	: 2017_08_27
=============================================================================
Updata

*****************************************************************************/
#ifndef SAVE_H
#define	SAVE_H
//////////////////////////////////////////////////////////////////////////////
//ヘッダーファイルインクルード
//////////////////////////////////////////////////////////////////////////////
#include "directory.h"

//////////////////////////////////////////////////////////////////////////////
//定数定義
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//Saveクラス定義
//////////////////////////////////////////////////////////////////////////////
class MotionEdit;

class Save : public Directory
{
public:
	Save(){}
	~Save(){}

	static string	selectSaveFileDir(HWND wnd, LPCSTR title, LPCSTR extension, LPCSTR extensionFilter);

	static void			saveTransform(FILE *fp, const Transform transform);
	static void			saveCollision(FILE *fp, Scene* scene);

private:
	static OPENFILENAME	mOpenFileName;					//名前を付けて保存の設定
	static char			mSaveFileDir[FILE_NAME_SIZE];	//ファイルの保存先
	static void			saveCollisionBox(FILE* fp, CollisionBox* col);
	static void			saveCollisionBall(FILE* fp, CollisionBall* col);
	static void			saveEachType(FILE* fp, Collision::TYPE type, Collision* col);
	static void			saveCollisionList(FILE* fp, const list<Collision*> colList);
};

class SaveMap : public Save
{
public:
	SaveMap(){}
	~SaveMap(){}

	static void saveFile(string fileName);

private:
	static void			saveObject(FILE *fp);
	static void			saveField(FILE *fp);
};
//
//class SaveMotion : public Save
//{
//public:
//	SaveMotion(){}
//	~SaveMotion(){}
//
//	static void saveFile(string fileName);
//
//private:
//	static void			savePartsName(FILE *fp, const Parts *part);
//	static void			savePartsPosture(FILE *fp, const Parts &part);
//	static void			savePartsInfomation(FILE * fp, const list<MotionData*>);
//};

class SaveCollision : public Save
{
public:
	SaveCollision() {}
	~SaveCollision() {}

	static void saveFile(string fileName, string saveMode);

private:
	static void	saveCollisionBox(FILE* fp, CollisionBox* col);
	static void	saveCollisionBall(FILE* fp, CollisionBall* col);
	static void	saveEachType(FILE* fp, Collision::TYPE type, Collision* col);
	static void	saveCollisionList(FILE* fp, const list<Collision*> colList);
	static void saveCollisionSceneHas(FILE* fp, list<Scene*> sceneList);
};

class SaveModel : public Save
{
public:
	SaveModel() {}
	~SaveModel() {}

	static void	saveFile(string fileName);

private:
	static void	saveModelName(FILE* fp, list<Scene*> sceneList);
	static void saveModelData(FILE* fp, list<Scene*> sceneList);
};

#endif // !SAVE_H
