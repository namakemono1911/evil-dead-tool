/*****************************************************************************
mortionModel.cpp
Aythor	: 上野　匠
Data	: 2017_12_08
=============================================================================
Updata

*****************************************************************************/
#ifndef MOTION_MODEL_H
#define MOTION_MODEL_H
//////////////////////////////////////////////////////////////////////////////
// ヘッダーインクルード
//////////////////////////////////////////////////////////////////////////////
#include "scene.h"
#include "motion.h"

//////////////////////////////////////////////////////////////////////////////
// 前方宣言
//////////////////////////////////////////////////////////////////////////////
class Scene;

//////////////////////////////////////////////////////////////////////////////
// クラス宣言
//////////////////////////////////////////////////////////////////////////////
//class MotionModel : public Scene
//{
//public:
//	MotionModel() {}
//	~MotionModel() {}
//
//	HRESULT		init(void);
//	void		uninit(void);
//	void		update(void);
//	void		draw(void);
//
//	static MotionModel*	create(string mortionFile);
//
//	auto		getParts(void) { return mMainParts; }
//	auto		getPartsAddr(void) { return &mMainParts; }
//
//private:
//	MotionData			mMainParts;
//
//};

#endif // !MORTION_MODEL_H
