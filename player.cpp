/*****************************************************************************
player.cpp
Aythor	: 上野　匠
Data	: 2017_06_15
=============================================================================
Updata

*****************************************************************************/
//////////////////////////////////////////////////////////////////////////////
//ファイルインクルード
//////////////////////////////////////////////////////////////////////////////
#include "main.h"
#include "manager.h"
#include "meshField.h"
#include "player.h"

//////////////////////////////////////////////////////////////////////////////
//関数名	: Player::create
//返り値	: 自身のポインタ
//説明		: 生成
//////////////////////////////////////////////////////////////////////////////
Player *Player::create(Player *player)
{
	Player *p = player;

	p->init();

	return p;
}

/*#################################################################################################################
PlayerEdit class
###################################################################################################################
*/
//////////////////////////////////////////////////////////////////////////////
//関数名	: PlayerEdit::init
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
HRESULT PlayerEdit::init(void)
{
	mMyName = "player";
	mSens = 0.001f;
	return S_OK;
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: PlayerEdit::uninit
//返り値	: 
//説明		: 終了
//////////////////////////////////////////////////////////////////////////////
void PlayerEdit::uninit(void)
{
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: PlayerEdit::update
//返り値	: 
//説明		: 更新
//////////////////////////////////////////////////////////////////////////////
void PlayerEdit::update(void)
{
	//カメラ情報取得
	Camera	*camera = Manager::getCamera();

	//入力デバイス情報
	auto	*mouse = Manager::getMouse();
	auto	*key = Manager::getKeyboard();

	//平行移動
	auto	vec = camera->getAt() - camera->getEye();
	auto	moveVec = Dvec3(0, 0, 0);
	if (key->getPress(DIK_W))		//前
		moveVec = vec;
	if (key->getPress(DIK_S))		//後ろ
		moveVec = vec * -1.f;
	if (key->getPress(DIK_A))		//左
		moveVec += vec3::DcrossProduct(vec, vec3(0.f, 1.f, 0.f));
	else if (key->getPress(DIK_D))	//右
		moveVec += vec3::DcrossProduct(vec3(0.f, 1.f, 0.f), vec);
	if (key->getPress(DIK_LSHIFT))
		moveVec = Dvec3(0.f, -1.f, 0.f);
	if (key->getPress(DIK_SPACE))
		moveVec = Dvec3(0, 1, 0);

	if (moveVec != Dvec3(0, 0, 0))
		moveVec = vec3::Dnormalize(moveVec);
	camera->moveEye(moveVec, 1.f);

	//視点操作
	float len = camera->getLen();
	vec2 cameraAngle = camera->getAngle();
	if (mouse->getPress(MOUSE_RIGHT))
	{
		cameraAngle.x += fmod(mouse->getMoving().x * mSens, PI);
		cameraAngle.y += -mouse->getMoving().y * mSens;

		//if (cameraAngle.y >= PI * 0.5f)			//仰角
		//	cameraAngle.y = PI * 0.5f;

		//if (cameraAngle.y <= PI)		//俯角
		//	cameraAngle.y = PI;

		camera->setAngle(cameraAngle);
		mTransform.rot = Dvec3(cameraAngle.x, cameraAngle.y, 0.f);

		//球面座標
		Dvec3 a = camera->getAt();
		vec3 eye = camera->getEye();
		float len = camera->getLen();
		vec3 at(
			len * cosf(-cameraAngle.y) * sinf(cameraAngle.x + PI) + eye.x,
			len * sinf(-cameraAngle.y) + eye.y,
			len * cosf(-cameraAngle.y) * cosf(cameraAngle.x + PI) + eye.z);
		camera->setPosAt(at);
	}
}
